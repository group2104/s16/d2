let numberA = null;
let numberB = null;
let operation = null;

//to retrieve an element from the webpage, we can use querySelector
let inputDisplay = document.querySelector('#txt-input-display');
//the document refers to the whole webpage and querySelector is used to select a specific object (HTML Elements)
//the querySelector function takes a string input that is formatted like a CSS Selector
//this allows us to get a specific element such as CSS selectors

// document.getElementById
// document.getElementsByClassName
// document.getElementsByTagName

//btnAdd
//btnSubtract
//btnMultiply
//btnDivide
//btnEqual
//btnDecimal
//btnClearAll
//btnBackspace

let btnNumbers = document.querySelectorAll('.btn-numbers');

let btnAdd = document.querySelector('#btn-add');
let btnSubtract = document.querySelector('#btn-subtract');
let btnMultiply = document.querySelector('#btn-multiply');
let btnDivide = document.querySelector('#btn-divide');
let btnEqual = document.querySelector('#btn-equal');
let btnDecimal = document.querySelector('#btn-decimal');

let btnClearAll = document.querySelector('#btn-clear-all');
let btnBackspace = document.querySelector('#btn-backspace');

//loop forEach()operation
btnNumbers.forEach(function(btnNumber){ 
    btnNumber.onclick = () => { //eventlistener
        inputDisplay.value += btnNumber.textContent; //once clicked, this will return
    }
});

//https://www.w3schools.com/jsref/prop_node_textcontent.asp

btnAdd.onclick = () => {
    if (numberA == null) {
        numberA = Number(inputDisplay.value); //displays value
        operation = 'addition';
        inputDisplay.value = null; //resets input display
    } 
    else if (numberB == null) {
        numberB = Number(inputDisplay.value); //read another num
        numberA = numberA + numberB; //get numA and add numB
        operation = 'addition';
        numberB.value = null; //clear the value
        inputDisplay.value = null; //answer will display
    }
}

btnSubtract.onclick = () => {
    if (numberA == null) {
        numberA = Number(inputDisplay.value); //displays value
        operation = 'subtraction';
        inputDisplay.value = null; //resets input display
    } 
    else if (numberB == null) {
        numberB = Number(inputDisplay.value); //read another num
        numberA = numberA - numberB; //get numA and subtract numB
        operation = 'subtraction';
        numberB.value = null; //clear the value
        inputDisplay.value = null; //answer will display
    }
}

btnMultiply.onclick = () => {
    if (numberA == null) {
        numberA = Number(inputDisplay.value);
        operation = 'multiplication';
        inputDisplay.value = null;
    } 
    else if (numberB == null) {
        numberB = Number(inputDisplay.value);
        numberA = numberA * numberB; 
        operation = 'multiplication';
        numberB.value = null;
        inputDisplay.value = null;
    }
}

btnDivide.onclick = () => {
    if (numberA == null) {
        numberA = Number(inputDisplay.value);
        operation = 'division';
        inputDisplay.value = null;
    } 
    else if (numberB == null) {
        numberB = Number(inputDisplay.value);
        numberA = numberA / numberB;
        operation = 'division';
        numberB.value = null;
        inputDisplay.value = null;
    }
}

btnClearAll.onclick = () => {
    numberA = null;
    numberB = null;
    operation = null;
    inputDisplay.value = null;
}

btnBackspace.onclick = () => {
    inputDisplay.value = inputDisplay.value.slice(0,-1); //delete last part of string NOT POP-it's for arrays
}

//https://www.w3schools.com/jsref/jsref_slice_array.asp

btnDecimal.onclick = () => {
    if (!inputDisplay.value.includes('.')) {
        inputDisplay.value = inputDisplay.value + btnDecimal.textContent;
    }
}

btnEqual.onclick = () => {
    if (numberB == null && inputDisplay.value !== ''){
        numberB = inputDisplay.value;
    }
    if (operation == 'addition') {
        inputDisplay.value = Number(numberA) + Number(numberB);
    }
    else if (operation == 'subtraction') {
        inputDisplay.value = Number(numberA) - Number(numberB);
    }
    else if (operation == 'multiplication') {
        inputDisplay.value = Number(numberA) * Number(numberB);
    }
    else if (operation == 'division') {
        inputDisplay.value = Number(numberA) / Number(numberB);
    }
}



/* ACTIVITY

Continue to create a condition for multiplication and division and equal

*/





let firstNameEl = document.querySelector('#firstName'); //make variable to get id firstName
let lastNameEl = document.querySelector('#lastName'); //a placeholder (variable) for id lastName

let fullNameEl = document.querySelector('#fullName'); //assign variable to selected id fullName

let myFunction = () => {
    let firstNameValue = firstNameEl.value; //make variable for input of value of firstName 
    let lastNameValue = lastNameEl.value; //make variable for input of value of lastName
    let fullNameValue = `${firstNameValue} ${lastNameValue}`; //make variable combination of firstName value and lastName value = fullName value
    fullNameEl.innerText = fullNameValue; //add text to fullNameEl the firstNameValue  + lastNameValue
}

firstNameEl.addEventListener('input', myFunction); //upon input of firstName, function will run
lastNameEl.addEventListener('input', myFunction); //upon input of lastName, function will run


